package com.javid.mvvmkotlin.dataBinding

import android.app.Application
import android.content.Context

/**
 * @author  : javid
 * @since   : 2020/Aug -- 9:52 AM
 * @summary : --
 */
 class Application : Application() {


    override fun onCreate() {
        super.onCreate()
    }
}