package com.javid.mvvmkotlin.dataBinding.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.javid.mvvmkotlin.dataBinding.data.api.ApiClient
import com.javid.mvvmkotlin.dataBinding.data.network.RestConnection
import com.javid.mvvmkotlin.dataBinding.model.Country
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * @author  : javid
 * @since   : 2020/Aug -- 12:00 PM
 * @summary : --
 */
class CountryListVM : ViewModel() {

    val dataList: MutableLiveData<List<Country>> = MutableLiveData()
    val restCon = RestConnection(apiInterFace = ApiClient.api)
    val disposable: CompositeDisposable = CompositeDisposable()


    fun getList(): MutableLiveData<List<Country>> {
        disposable.add(
            restCon.getCountryList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Country>>() {
                    override fun onSuccess(t: List<Country>?) {
                        for (country in t!!) {
                            country.url = "https://www.countryflags.io/${country.Iso}/flat/64.png"
                        }
                        dataList.value = t
                    }

                    override fun onError(e: Throwable?) {
                        Log.d("onError: ", e?.message.toString())
                    }
                })
        )
        return dataList
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }
}