package com.javid.mvvmkotlin.dataBinding.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author  : javid
 * @since   : 2020/Aug -- 11:03 AM
 * @summary : --
 */
@Entity(tableName = "country")
data class Country(
    @PrimaryKey(autoGenerate = true) var id: Int=0,
    @ColumnInfo(name = "CountryName") val CountryName: String,
    @ColumnInfo(name = "CountryCode") val CountryCode: String,
    @ColumnInfo(name = "url") var url: String,
    @ColumnInfo(name = "Iso") val Iso: String
) {
}
