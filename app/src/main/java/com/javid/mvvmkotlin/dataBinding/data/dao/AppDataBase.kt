package com.javid.mvvmkotlin.dataBinding.data.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.javid.mvvmkotlin.dataBinding.model.Country

/**
 * @author  : javid
 * @since   : 2020/Aug -- 3:13 PM
 * @summary : --
 */
@Database(entities = [Country::class], version = 1)
abstract class AppDataBase : RoomDatabase() {

    abstract fun dao(): TodoDao

    companion object {
        private var instance: AppDataBase? = null
        private val LOCK = Any()
        fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDB(context).also { instance = it }
        }

        private  fun buildDB(context: Context): AppDataBase =
            Room.databaseBuilder(context.applicationContext, AppDataBase::class.java, "test-db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
    }
}