package com.javid.mvvmkotlin.dataBinding.data.api

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author  : javid
 * @since   : 2020/Aug -- 11:00 AM
 * @summary : --
 */
object ApiClient {
   private val BASE_URL: String = "http://javiddev.ir/webService/"
    val api: ApiInterFace
   private val retrofit: Retrofit

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        api = retrofit.create(ApiInterFace::class.java)
    }
}