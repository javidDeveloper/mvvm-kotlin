package com.javid.mvvmkotlin.dataBinding.data.api

import com.javid.mvvmkotlin.dataBinding.model.Country
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

/**
 * @author  : javid
 * @since   : 2020/Aug -- 10:53 AM
 * @summary : --
 */
interface ApiInterFace {

    //    http://javiddev.ir/webService/publicService/getCountryList.php
    @GET("publicService/getCountryList.php")
    fun getCountryList(): Single<List<Country>>
}