package com.javid.mvvmkotlin.dataBinding.model

/**
 * @author  : javid
 * @since   : 2020/Aug -- 12:02 PM
 * @summary : --
 */
data class Users(
    val name: String,
    val url: String
) {
    companion object {
        //        val listUsers = listOf<Users>()
        fun getListUsers(): List<Users> {
            val listUsers = mutableListOf<Users>()
            listUsers.add(Users("name_1", "url_1"))
            listUsers.add(Users("name_2", "url_2"))
            listUsers.add(Users("name_3", "url_3"))
            listUsers.add(Users("name_4", "url_4"))
            return listUsers
        }
    }
}