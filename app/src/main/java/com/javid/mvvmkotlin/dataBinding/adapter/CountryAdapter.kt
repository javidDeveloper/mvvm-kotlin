package com.javid.mvvmkotlin.dataBinding.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.javid.mvvmkotlin.R
import com.javid.mvvmkotlin.dataBinding.model.Country
import com.javid.mvvmkotlin.databinding.CountryItemBinding

/**
 * @author  : javid
 * @since   : 2020/Aug -- 2:44 PM
 * @summary : --
 */
class CountryAdapter(
    private val context: Context,
    private val userList: List<Country>,
    private val onItemClick: OnItemClick
) :
    RecyclerView.Adapter<CountryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val view =
            DataBindingUtil.inflate<CountryItemBinding>(
                layoutInflater,
                R.layout.country_item,
                parent,
                false
            )
        return ViewHolder(view)
    }

    override fun getItemCount() = userList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(userList[position])
        holder.itemView.setOnClickListener {
            onItemClick.onClick(userList[position])
//            onItemClick.onClick { userList[position] }
        }
    }

    inner class ViewHolder(private val binding: CountryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: Country) {
            binding.model = model
        }
    }

    open interface OnItemClick {
        fun onClick(item: Country)
    }
}
