package com.javid.mvvmkotlin.dataBinding.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.javid.mvvmkotlin.dataBinding.model.Country
import com.javid.mvvmkotlin.databinding.FragmentDetailMovieBinding

class DetailMovieFragment : Fragment() {
    lateinit var mBinding: FragmentDetailMovieBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val country = Country(
            0,
            arguments?.getString("CountryName")!!,
            arguments?.getString("CountryCode")!!,
            arguments?.getString("url")!!,
            arguments?.getString("Iso")!!
        )
        mBinding.model = country
    }

}