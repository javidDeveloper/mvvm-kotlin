package com.javid.mvvmkotlin.dataBinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

/**
 * @author  : javid
 * @since   : 2020/Aug -- 9:12 AM
 * @summary : --
 */


    @BindingAdapter("bind:imgUrl")
    fun imageUrl(imageView: ImageView, url: String) {
        Picasso.get().load(url).into(imageView)
    }

