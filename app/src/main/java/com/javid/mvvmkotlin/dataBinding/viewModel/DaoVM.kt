package com.javid.mvvmkotlin.dataBinding.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.javid.mvvmkotlin.dataBinding.data.dao.AppDataBase
import com.javid.mvvmkotlin.dataBinding.data.dao.TodoDao
import com.javid.mvvmkotlin.dataBinding.model.Country

/**
 * @author  : javid
 * @since   : 2020/Aug -- 5:01 PM
 * @summary : --
 */
class DaoVM(application: Application) : AndroidViewModel(application) {
    private val dbDao = AppDataBase
    private val dao: TodoDao = dbDao.invoke(application.applicationContext).dao()
    private val countryList: LiveData<List<Country>> = dao.getAllCountryList()

    fun getCountryList(): LiveData<List<Country>> {
        return countryList
    }
}