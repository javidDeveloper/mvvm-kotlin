package com.javid.mvvmkotlin.dataBinding.fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.javid.mvvmkotlin.R
import com.javid.mvvmkotlin.dataBinding.adapter.CountryAdapter
import com.javid.mvvmkotlin.dataBinding.model.Country
import com.javid.mvvmkotlin.dataBinding.viewModel.CountryListVM
import com.javid.mvvmkotlin.databinding.FragmentHomeBinding

class HomeFragment : Fragment(), CountryAdapter.OnItemClick {

    lateinit var mBinding: FragmentHomeBinding
    lateinit var countryListVM: CountryListVM
    lateinit var adapter: CountryAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        countryListVM = ViewModelProvider(this).get(CountryListVM::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        countryListVM.getList().observe(viewLifecycleOwner, Observer {
            mBinding.rlc.layoutManager =
                StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
            mBinding.rlc.setHasFixedSize(true)
            adapter = context?.let { it1 -> CountryAdapter(it1, userList = it, onItemClick = this) }!!
            mBinding.rlc.adapter = adapter

        })
    }

    override fun onClick(item: Country) {
        val bundle = Bundle()
        bundle.putString("CountryName", item.CountryName)
        bundle.putString("CountryCode", item.CountryCode)
        bundle.putString("url", item.url)
        bundle.putString("Iso", item.Iso)
        Navigation.findNavController(mBinding.rlc)
            .navigate(R.id.action_homeFragment_to_detailMovieFragment2, bundle)
    }
}