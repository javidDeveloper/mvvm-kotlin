package com.javid.mvvmkotlin.dataBinding.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.javid.mvvmkotlin.R
import com.javid.mvvmkotlin.dataBinding.model.Users
import com.javid.mvvmkotlin.databinding.RlcItemBinding

/**
 * @author  : javid
 * @since   : 2020/Aug -- 2:44 PM
 * @summary : --
 */
class RecyclerViewAdapter(
    private val context: Context,
    private val userList: List<Users>
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val view =
            DataBindingUtil.inflate<RlcItemBinding>(
                layoutInflater,
                R.layout.rlc_item,
                parent,
                false
            )
        return ViewHolder(view)
    }

    override fun getItemCount() = userList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(userList[position])
    }

    inner class ViewHolder(private val binding: RlcItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: Users) {
            binding.model = model
        }
    }
}
