package com.javid.mvvmkotlin.dataBinding.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.javid.mvvmkotlin.dataBinding.model.Country

/**
 * @author  : javid
 * @since   : 2020/Aug -- 3:00 PM
 * @summary : --
 */
@Dao
interface TodoDao {
    @Query("select * from COUNTRY")
    abstract fun getAllCountryList(): LiveData<List<Country>>

    @Query("select * from COUNTRY where Iso = :iso")
    abstract fun getCountryByIso(iso: String): LiveData<Country>

    @Insert
    abstract fun addCountry(country: Country)

    @Update
    abstract fun updateCountry(country: Country)

    @Delete
    abstract fun deleteCountry(country: Country)
}