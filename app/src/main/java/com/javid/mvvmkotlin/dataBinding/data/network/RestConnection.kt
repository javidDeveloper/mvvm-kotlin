package com.javid.mvvmkotlin.dataBinding.data.network

import com.javid.mvvmkotlin.dataBinding.data.api.ApiClient
import com.javid.mvvmkotlin.dataBinding.data.api.ApiInterFace
import com.javid.mvvmkotlin.dataBinding.model.Country
import io.reactivex.rxjava3.core.Single

/**
 * @author  : javid
 * @since   : 2020/Aug -- 11:20 AM
 * @summary : --
 */
class RestConnection(
    val apiInterFace: ApiInterFace
) {
    fun getCountryList(): Single<List<Country>>
    {
      return apiInterFace.getCountryList()
    }

}